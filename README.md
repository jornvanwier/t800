# T800, like terminator

Simple wrapper around tmux to make it easy to SSH into multiple servers, without
terminator getting the way.

# Usage
On first launch bare configuration files are created in `~/.config/t800`.

`targets.txt` contains the options for servers to SSH into. Brace expansion is supported.

`spells.txt` contains the commands to execute.
 
After both configuration files are filled, its as simple as executing `t800`, selecting the target and command to execute and you're in. 

If tmux briefly flashes on the screen you might have made a mistake in your 
targets configuration. There's not much error handling if connecting to the 
server fails.

# Requirements
- tmux
- fzf
- sed